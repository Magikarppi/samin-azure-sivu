FROM node

WORKDIR /app

COPY . .

RUN npm install

ARG PORT=3003
ARG VERSION=0.0

ENV PORT=${PORT}
ENV VERSION=${VERSION}

EXPOSE ${PORT}

CMD [ "node", "index.js" ]