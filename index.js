import express from 'express';
import dotenv from 'dotenv';

dotenv.config();

const server = express();

const { PORT, VERSION } = process.env;

server.get('/', (_req, res) => {
    res.status(200).send(`Helloooooooooo! (v. ${VERSION})`);
});

server.listen(process.env.PORT, () => {
    console.log(`Server v. ${VERSION} listening on port ${PORT}`);
});
